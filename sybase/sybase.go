package sybase

import (
	"database/sql"
	"fmt"
	"log"
	"strings"

	// import sybase driver
	_ "github.com/thda/tds"
)

const (
	driver      string = "tds"
	execCommand string = "EXEC"
	Username           = ""
	Password           = ""
	Server             = ""
	Database           = ""
)

const Port = 4350

// ExecQueryBySprocName takes sproc name and executes the sproc
func ExecQueryBySprocName(sprocName string, params ...interface{}) (result []map[string]interface{}, err error) {
	queryStatement := getQueryStatement(sprocName, params...)
	return connectAndExecQuery(queryStatement, params)
}

// ExecQueryByCommand takes the prepared query and executes the query
func ExecQueryByCommand(spExecStatement string, params ...interface{}) (result []map[string]interface{}, err error) {
	queryStatement := formatQuery(spExecStatement, params...)
	return connectAndExecQuery(queryStatement, params)
}

func connectAndExecQuery(queryStatement string, params ...interface{}) (result []map[string]interface{}, err error) {
	db, err := openConnection()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	rows, err := db.Query(queryStatement)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer rows.Close()

	columns, err := rows.Columns()
	if err != nil {
		fmt.Println(err.Error())
		return
	}

	records := scanRecords(columns, rows)

	err = rows.Err()
	if err != nil {
		log.Fatal(err)
	}

	return records, nil
}

func openConnection() (*sql.DB, error) {
	connectionString := getConnectionString()
	db, err := sql.Open(driver, connectionString)
	if err != nil {
		return db, err
	}
	return db, nil
}

func getQueryStatement(sprocName string, params ...interface{}) string {
	queryStatement := prepareExecStatement(sprocName, params...)
	return formatQuery(queryStatement, params...)
}

func formatQuery(queryStatement string, params ...interface{}) string {
	if strings.Contains(queryStatement, "%") && params != nil && len(params) > 0 {
		queryStatement = fmt.Sprintf(queryStatement, params...)
	}
	return queryStatement
}

func prepareExecStatement(sprocName string, params ...interface{}) string {
	spExecStatement := execCommand + " " + sprocName + " "
	for _, v := range params {
		switch v.(type) {
		case string:
			spExecStatement += "'%s' "
		case int:
			spExecStatement += "%d "
		default:
			spExecStatement += " "
		}
	}
	return spExecStatement
}

func getConnectionString() string {
	// Connection String Format - tds://username:password@host:port/database?parameter=value&parameter2=value2
	connectionString := fmt.Sprintf("tds://%[1]s:%[2]s@%[3]s:%[4]d/%[5]s", Username, Password, Server, Port, Database)

	// connectionString := fmt.Sprintf("tds://%[1]s:%[2]s@%[3]s:%[4]d/%[5]s?applicationName=%[6]s", config.Username, config.Password, config.Server, config.Port, config.Database, config.ApplicationName)
	return connectionString
}

func scanRecords(columns []string, rows *sql.Rows) []map[string]interface{} {

	rowValues := make([]interface{}, len(columns))   // a slice of interface{}'s to represent values of each row.
	valPointers := make([]interface{}, len(columns)) // a slice of interface{}'s to contain pointers to each item in the rowValues slice.
	for i := 0; i < len(columns); i++ {
		valPointers[i] = &rowValues[i]
	}

	records := make([]map[string]interface{}, 0)

	// loop rows
	for rows.Next() {

		// scan row to valPointers
		if err := rows.Scan(valPointers...); err != nil {
			log.Fatal(err)
		}

		// row vals
		row := make(map[string]interface{})
		for i, v := range rowValues {
			row[columns[i]] = v
		}
		records = append(records, row)
	}

	return records
}
